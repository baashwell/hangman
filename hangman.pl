#!/usr/bin/perl
use strict;
use warnings;
use Tk;

#Ben Ashwell Hangman Game.

#Here I am creating my variables to use throughout the program. they are:
#guessWord is the array to hold the array to hold the indiv letters of the guess word
#letter is just a variable to hold the letters to examine them individually
#curWord is an array to hold what is currently in the answer box
#wrongTries is the amount of wrong tries the person has had
#word is the word to be guessed as a string
my @guessWord;
my $letter;
my @curWord;
my $wrongTries = 0;
my $word;

#This is the TK part. This is where I create the canvas to the right and also make all buttons and labels and place
#Them on the window.
my $main = new MainWindow;
$main -> configure(-title=>"Hangman");

		#The canvas is where the picture of hangman will be placed.
		my $canvas = $main -> Canvas;
		$canvas -> configure(-height=>500, -width=>250, -relief=>'sunken', -bd=>2, -background=>'white');
		$canvas -> pack(-side=>'right');
	
		#all of the labels etc are self explanatory, the -pady is the padding in the y direction to spread it out a bit.
		my $title = $main -> Label(-text=>'Hangman!', -pady=>10);
		$title -> pack;
		
		my $start = $main -> Button(-text=>'Start', -command=>sub{StartHang()}, -pady=>10);
		$start -> pack;
		
		my $HML = $main -> Label(-text=>'How Many Letters?', -pady=>10);
		$HML -> pack;
		
		my $letBox = $main -> Entry(-width=>5, -text=>'4');
		$letBox -> pack(-pady=>10);
		
		my $guess = $main -> Label(-text=>'Guess a letter :D', -pady=>10);
		$guess -> pack;
		
		my $guessBox = $main-> Entry(-width=>5);
		$guessBox -> pack(-pady=>10);
		
		my $guessButton = $main -> Button(-text=>'Try!', -command=>sub{GuessLetter()}, -pady=>10, -state=> 'disabled');
		$guessButton ->pack;
		
		my $answer = $main -> Entry(-width=> 20);
		$answer -> pack(-pady=>10);
		
		my $result = $main ->Label(-text=>"Please enter a number\nof letters between\n3 and 10", -pady=>10);
		$result -> pack;
		
MainLoop;

#This subroutine loads the dictionairy, and chooses an random word from it and then places underscores in the answer box.
sub StartHang
{
	#Firstly, I clear all the variables so that if this restarting from a past game it is fresh
	@curWord = "";
	$wrongTries= 0;
	$answer -> delete(0, 'end');
	$result -> configure(-text=>"Please enter a number of letters between 3 and 10");
	$canvas -> delete('all');
	
	#this activate the try button and disables the start button.
	$guessButton -> configure(-state=>'active');
	$start -> configure(-state=>'disabled');
	
	
	open(HANDLE, 'dict.txt')||
                        die "Cannot open this file, $!\n";

	my @dictionary = <HANDLE>;
	
	#these variables are here for later use in this subroutine they are:
	#letAmount is the amount of letters entered in the box.
	#rWAmount is the amount of letters in the randomword.
	my $letAmount = $letBox ->get();
	
	$result -> configure(-text=>"");
	my $rWAmount = 0;
	
	#while the amount of letters wanted is not the same as the random word letter amount, keep on getting new words untill we have one that is correct length.
	while($letAmount != $rWAmount)
	{
		my $randomLocation = int(rand @dictionary);
		$word = $dictionary[$randomLocation];
		chomp $word;
		$rWAmount = length $word;
	}
		
	#This splits up the string from the dictionary and places each letter into the array guessWord
	@guessWord = split(/ */, $word);
	
	#this makes curWord gain a _ for each letter in the random word.
	foreach $letter (@guessWord)
	{
		push(@curWord, "_");
	}
	$answer -> insert(0, "@curWord");	
}

#This subroutine will run through each letter in the random chosen word, comparing it to the guessed letter.
#If they are the same it will 
sub GuessLetter
{
	#these variables are set to be used in this subroutine. They are:
	#compLet is the letter guessed
	#Count is so I can keep count of what letter of the guess word I am at and to be able to change it in the array
	#changes is the amount of changes of letters occured
	#changes2 is the amount of _ left in the guess word
	my $compLet = $guessBox ->get();
	my $count = 0;
	my $changes = 0;
	my $changes2 = 0;
	
	foreach $letter (@guessWord)
	{
		#increase where we are in the guess word, if the letters are the same swap the _ for the letter.
		#and then increase changes for later use
		$count ++;
		if($compLet eq $letter)
		{
			$curWord[$count] = "$letter";
			$changes ++;
		}
	}
	
	#delete what is in answer currently and place the new answer in.
	$answer -> delete(0, 'end');
	$answer -> insert(0, "@curWord");
	
	foreach $letter (@curWord)
	{
		#if a letter in the answer is a _ then the word is not correct so increase changes2 for later use
		if($letter eq "_")
		{
			$changes2 ++
		}
	}
	
	#if there are no _ then they have guessed all the word.
	if($changes2 == 0)
	{
		$result -> configure(-text => "Congradulations you guessed it!\nPlease play again.");
		$guessBox -> delete(0,'end');
		$guessButton -> configure(-state=>'disabled');
		$start -> configure(-state=>'active');
	}
	
	#if there have been no changes to the actual word then add a line to the hangman
	if($changes == 0)
	{
		$guessBox -> delete(0,'end');	
		AddLine();
	}
	$guessBox -> delete(0,'end');
}

#This is the subroutine to add a line to the hangman count down.
sub AddLine
{
	#if the hangman has not already been drawn, draw the appropriate line and then increase the amount of wrong tries.
	#but if there have already been 11 wrong tries then they have had too many wrong guesses.
	if($wrongTries < 12)
	{
		#horizontal line at bottom
		if($wrongTries == 0)
		{
			$canvas -> create('line', 225, 450, 25, 450, -width=> 2, -fill=> 'black');
		}
		#vertical line at left side
		if($wrongTries == 1)
		{
			$canvas -> create('line', 25, 450, 25, 50, -width=> 2, -fill=> 'black');
		}
		#diagonal line at bottom left
		if($wrongTries == 2)
		{
			$canvas -> create('line', 75, 450, 25, 400, -width => 2, -fill=> 'black');
		}
		#horizontal line at top
		if($wrongTries == 3)
		{
			$canvas -> create('line', 25, 50, 175, 50, -width => 2, -fill=> 'black');
		}
		#diagonal line at top left
		if($wrongTries == 4)
		{
			$canvas -> create('line', 25, 100, 75, 50, -width => 2, -fill=> 'black');
		}
		#vertical line down to hangman
		if($wrongTries == 5)
		{
			$canvas -> create('line', 175, 50, 175, 200, -width => 2, -fill=> 'black');
		}
		#hangmans head
		if($wrongTries == 6)
		{
			$canvas -> create('oval', 165, 200, 185, 220, -width => 2, -fill=> 'black');
		}
		#hangmans body
		if($wrongTries == 7)
		{
			$canvas -> create('line', 175, 200, 175, 270, -width => 2, -fill=> 'black');
		}
		#hangmans left arm
		if($wrongTries == 8)
		{
			$canvas -> create('line', 175, 230, 150, 245, -width => 2, -fill=> 'black');
		}
		#hangmans right arm
		if($wrongTries == 9)
		{
			$canvas -> create('line', 175, 230, 200, 245, -width => 2, -fill=> 'black');
		}
		#hangmans left leg
		if($wrongTries == 10)
		{
			$canvas -> create('line', 175, 270, 150, 300, -width => 2, -fill=> 'black');
		}
		#hangmans right leg
		if($wrongTries == 11)
		{
			$canvas -> create('line', 175, 270, 200, 300, -width => 2, -fill=> 'black');
		}
		
		#increases the amount of wrong tries so that it will draw different each time
		$wrongTries ++;
	}
	else
	{
		$result -> configure(-text=>"You have failed!!\nYou had too many guesses.\nThe word was " . $word . "\nPlease Try again.");
		$guessButton -> configure(-state=>'disabled');
		$start -> configure(-state=>'active');
	}
}
