Ben Ashwell Hangman READ ME

To run the game go to the command line of that directory and just type perl hangman.pl.

When the game comes up choose the amount of letters you would like to guess between and then click start, 
the try button will now be enabled.

Now enter the letter you wish to guess and click try, you will either get a line or it will show the letter in
the answer box at the scren.

Contnue with this untill you either:

a) Guess the word in which case a congratualtions message will appear and you can click start again to play again.

b) you will use all your lives and a failure message will appear telling you the word you where meant to be
 guessing and you can click start to play again.

Hope you enjoy.
Many thanks
Ben Ashwell.
